package com.example.al.menu;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

	boolean[] checked = {false, false};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/*
		 * назначаем виджету контекстное меню
		 */
//		registerForContextMenu( findViewById(R.id.textView) );
		findViewById(R.id.textView).setOnCreateContextMenuListener(this);
	}

	/*
	 * переопределяем для появления сендвич-кнопки с главным меню
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);

		/*
		 * динамически добавляем пункты меню
		 */
		menu.add("menu-1");
		menu.add("menu-2");

		return super.onCreateOptionsMenu(menu);
	}

	/*
	 * обработчик события выбора элемента в главном меню
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*
		 * получаем название выбранного пункта меню
		 */
		String message = item.getTitle().toString();

		/*
		 * если пункт меню - чекбокс, то меняем его состояние (это делается вручную)
		 */
		if ( item.isCheckable() ) {
			item.setChecked( !item.isChecked() );
			message += item.isChecked() ? " checked" : " unchecked";
		}
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

		return super.onOptionsItemSelected(item);
	}

	/*
	 * переопределяем для появления контекстного меню
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		getMenuInflater().inflate(R.menu.main_menu, menu);

		/*
		 * задаём начальное состояние чекбоксов, которое хранится в поле this.checked
		 * (поскольку чекбоксы будут сброшены после закрытия контекстного меню)
		 */
		menu.findItem(R.id.menu_0).setChecked(checked[0]);
		menu.findItem(R.id.menu_1).setChecked(checked[1]);

		super.onCreateContextMenu(menu, v, menuInfo);
	}

	/*
	 * обработчик события выбора элемента в контекстном меню
	 */
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		/*
		 * получаем название выбранного пункта меню
		 */
		String message = item.getTitle().toString();

		/*
		 * если пункт меню - чекбокс, то меняем его состояние (это делается вручную)
		 */
		if ( item.isCheckable() ) {
			item.setChecked( !item.isChecked() );
			message += item.isChecked() ? " checked" : " unchecked";

			/*
			 * сохраняем состояние чекбокса в поле this.checked
			 */
			int itemId = item.getItemId();
			if (itemId == R.id.menu_0) {
				checked[0] = item.isChecked();
			} else if (itemId == R.id.menu_1) {
				checked[1] = item.isChecked();
			}
		}
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

		return super.onContextItemSelected(item);
	}
}
